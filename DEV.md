# For developers

- rbot work on [telegroo](https://gitlab.com/teslex/telegroo)
- [telegroo docs](https://gitlab.com/teslex/telegroo/blob/master/DOCS.md)


## Instruction

**First, fork or create new branch**

#### Create command

- Create new command class:
```groovy
@Component
class MyCommand extends RCommand {
	MyCommand() {
		super(/my/)
	}

	@Override
	def execute(Map update, Matcher matcher, RBot bot) {
		bot.telegroo.sendMessage('Hello, World!')
	}
}