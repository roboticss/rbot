package tech.teslex.rbot.bot.event.newuser

import groovy.transform.CompileStatic
import org.springframework.context.ApplicationEvent

@CompileStatic
class RBotNewUserEvent extends ApplicationEvent {
	/**
	 * Create a new ApplicationEvent.
	 * @param source the object on which the event initially occurred (never {@code null})
	 */
	RBotNewUserEvent(Object source) {
		super(source)
	}
}
