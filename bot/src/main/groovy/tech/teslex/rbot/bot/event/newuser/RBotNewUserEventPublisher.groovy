package tech.teslex.rbot.bot.event.newuser

import groovy.transform.CompileStatic
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.ApplicationEventPublisherAware
import org.springframework.stereotype.Component

@CompileStatic
@Component
class RBotNewUserEventPublisher implements ApplicationEventPublisherAware {

	private ApplicationEventPublisher publisher

	@Override
	void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
		this.publisher = applicationEventPublisher
	}

	void publish(RBotNewUserEvent newUserEvent) {
		this.publisher.publishEvent(newUserEvent)
	}
}
