package tech.teslex.rbot.bot.event.stop

import groovy.transform.CompileStatic
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.ApplicationEventPublisherAware
import org.springframework.stereotype.Component

@CompileStatic
@Component
class RBotStopEventPublisher implements ApplicationEventPublisherAware {

	private ApplicationEventPublisher publisher

	@Override
	void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
		this.publisher = applicationEventPublisher
	}

	void publish(RBotStopEvent stopEvent) {
		this.publisher.publishEvent(stopEvent)
	}
}
