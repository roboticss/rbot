package tech.teslex.rbot.bot.command

import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import tech.teslex.rbot.bot.RBot
import tech.teslex.rbot.bot.command.base.RBotCommand
import tech.teslex.rbot.data.model.User
import tech.teslex.rbot.data.service.user.UserService

@CompileStatic
@Component
class MeCommand extends RBotCommand {

	@Autowired
	UserService userService

	MeCommand() {
		super(/me/)
	}

	@Override
	def execute(Map update, RBot bot) {
		Long fromId = update['message']['from']['id'] as Long

		Optional<User> user = userService.findById(fromId)

		if (user) {
			bot.telegroo.reply("""

*${user.get().username}${user.get().isAdmin ? ' (admin)' : ''}*:
 - Joined: `${user.get().joinedAt}`
 - Updated: `${user.get().updatedAt}`

""".trim(), [parse_mode: 'Markdown'])
		}
	}
}
