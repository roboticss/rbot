package tech.teslex.rbot.bot

import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import tech.teslex.telegroo.Telegroo
import tech.teslex.rbot.bot.command.base.RBotCommand
import tech.teslex.rbot.bot.event.RBotEventSource
import tech.teslex.rbot.bot.event.start.RBotStartEvent
import tech.teslex.rbot.bot.event.start.RBotStartEventPublisher
import tech.teslex.rbot.bot.event.stop.RBotStopEvent
import tech.teslex.rbot.bot.event.stop.RBotStopEventPublisher

import java.util.regex.Matcher

@CompileStatic
@Component
class RBot {

	boolean active

	Telegroo telegroo

	@Value('${rbot.token}')
	String token

	@Value('${rbot.webhookUrl}')
	String webhookUrl

	@Autowired
	List<RBotCommand> commands

	@Autowired
	RBotStartEventPublisher startEventPublisher
	@Autowired
	RBotStopEventPublisher stopEventPublisher

	void init() {
		telegroo = new Telegroo(token)
		telegroo.catchException = { Exception e ->
			e.printStackTrace()
			stop()
		}
		commands.each { command it }
	}

	void start() {
		init()
		try {
			telegroo.setWebhook(webhookUrl)
			telegroo.active = true
			telegroo.isWebhook = true
			this.active = true

			startEventPublisher.publish(new RBotStartEvent(new RBotEventSource(this)))
		} catch (e) {
			e.printStackTrace()
		}
	}

	void stop() {
		telegroo.stop()
		stopEventPublisher.publish(new RBotStopEvent(new RBotEventSource(this)))
	}

	def command(RBotCommand command) {
		telegroo.onCommand command.command, { command.execute(it['update'] as Map, it['match'] as Matcher, this) }
	}

	def message(RBotCommand command) {
		telegroo.onMessage command.command, { command.execute(it['update'] as Map, it['match'] as Matcher, this) }
	}
}
