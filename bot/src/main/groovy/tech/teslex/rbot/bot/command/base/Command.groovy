package tech.teslex.rbot.bot.command.base

import groovy.transform.CompileStatic

import java.util.regex.Matcher

@CompileStatic
interface Command {

	def execute(Map update, Matcher matcher)

	def execute(Matcher matcher)

	def execute()
}
