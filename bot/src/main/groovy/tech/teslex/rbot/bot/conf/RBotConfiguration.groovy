package tech.teslex.rbot.bot.conf

import groovy.json.JsonSlurper
import groovy.transform.CompileStatic
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@CompileStatic
@Configuration
class RBotConfiguration {

	@Bean
	JsonSlurper jsonSlurper() {
		return new JsonSlurper()
	}
}
