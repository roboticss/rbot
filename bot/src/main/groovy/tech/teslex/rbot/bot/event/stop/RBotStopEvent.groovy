package tech.teslex.rbot.bot.event.stop

import groovy.transform.CompileStatic
import org.springframework.context.ApplicationEvent

@CompileStatic
class RBotStopEvent extends ApplicationEvent {
	/**
	 * Create a new ApplicationEvent.
	 * @param source the object on which the event initially occurred (never {@code null})
	 */
	RBotStopEvent(Object source) {
		super(source)
	}
}
