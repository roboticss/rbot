package tech.teslex.rbot.bot.event.start

import groovy.transform.CompileStatic
import org.springframework.context.ApplicationEvent

@CompileStatic
class RBotStartEvent extends ApplicationEvent {
	/**
	 * Create a new ApplicationEvent.
	 * @param source the object on which the event initially occurred (never {@code null})
	 */
	RBotStartEvent(Object source) {
		super(source)
	}
}
