package tech.teslex.rbot.bot.event.start

import groovy.transform.CompileStatic
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.ApplicationEventPublisherAware
import org.springframework.stereotype.Component

@CompileStatic
@Component
class RBotStartEventPublisher implements ApplicationEventPublisherAware {

	private ApplicationEventPublisher publisher

	@Override
	void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
		this.publisher = applicationEventPublisher
	}

	void publish(RBotStartEvent startEvent) {
		this.publisher.publishEvent(startEvent)
	}
}
