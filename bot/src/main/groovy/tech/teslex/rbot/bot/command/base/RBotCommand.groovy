package tech.teslex.rbot.bot.command.base

import groovy.transform.CompileStatic
import tech.teslex.rbot.bot.RBot

import java.util.regex.Matcher

@CompileStatic
abstract class RBotCommand implements Command {

	String command

	RBotCommand(String command) {
		this.command = command
	}

	RBotCommand() {
	}

	def execute() {}

	def execute(RBot bot) {}

	def execute(Map update) { execute() }

	def execute(Map update, RBot bot) { execute(bot) }

	def execute(Matcher matcher) { execute() }

	def execute(Matcher matcher, RBot bot) { execute(bot) }

	def execute(Map update, Matcher matcher) { execute(update); execute(matcher) }

	def execute(Map update, Matcher matcher, RBot bot) { execute(update, bot); execute(matcher, bot) }

}
