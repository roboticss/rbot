package tech.teslex.rbot.bot.event.handles

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationListener
import org.springframework.stereotype.Component
import tech.teslex.rbot.bot.event.start.RBotStartEvent

@Component
class RBotStartHandler implements ApplicationListener<RBotStartEvent> {

	private static final Logger logger = LoggerFactory.getLogger(RBotStartHandler)

	@Override
	void onApplicationEvent(RBotStartEvent event) {
		logger.info('RBot was started!')
	}
}
