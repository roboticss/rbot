package tech.teslex.rbot.bot.command

import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import tech.teslex.rbot.bot.RBot
import tech.teslex.rbot.bot.command.base.RBotCommand
import tech.teslex.rbot.bot.event.RBotEventSource
import tech.teslex.rbot.bot.event.newuser.RBotNewUserEvent
import tech.teslex.rbot.bot.event.newuser.RBotNewUserEventPublisher
import tech.teslex.rbot.data.model.User
import tech.teslex.rbot.data.service.user.UserService

@CompileStatic
@Component
class RBotCommand extends RBotCommand {

	@Autowired
	UserService userService

	@Autowired
	RBotNewUserEventPublisher newUserEventPublisher

	RBotCommand() {
		super(/start/)
	}

	@Override
	def execute(Map update, RBot bot) {
		boolean fromIsBot = update['message']['from']['is_bot']

		if (!fromIsBot) {
			Long fromId = update['message']['from']['id'] as Long
			String fromUsername = update['message']['from']['username']

			if (userService.exists(fromId)) {
				bot.telegroo.sendMessage('З поверненням!')
			} else {
				newUserEventPublisher.publish(new RBotNewUserEvent(new RBotEventSource(bot)))
				userService.save(new User(id: fromId, username: fromUsername, isAdmin: fromUsername == 'expexes'))
				bot.telegroo.sendMessage('Привіт!')
			}
		}
	}
}
