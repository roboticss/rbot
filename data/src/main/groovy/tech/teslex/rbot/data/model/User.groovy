package tech.teslex.rbot.data.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.CompileStatic

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@CompileStatic
@Entity
@Table(name = 'users')
class User {

	@Id
	Long id

	String username

	@Column(name = 'joined_at')
	Long joinedAt = new Date().time

	@Column(name = 'updated_at')
	Long updatedAt = new Date().time

	@Column(name = 'is_admin')
	boolean isAdmin = false

	Date getJoinedAt() {
		return new Date(joinedAt)
	}

	@JsonIgnore
	void setJoinedAt(Date joinedAt) {
		this.joinedAt = joinedAt.time
	}

	Date getUpdatedAt() {
		return new Date(updatedAt)
	}

	@JsonIgnore
	void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt.time
	}
}
