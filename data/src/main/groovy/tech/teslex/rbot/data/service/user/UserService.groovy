package tech.teslex.rbot.data.service.user

import groovy.transform.CompileStatic
import tech.teslex.rbot.data.model.User

@CompileStatic
interface UserService {

	User save(User user)

	void delete(Long id)

	Boolean exists(Long id)

	Optional<User> findById(Long id)

	Optional<User> findByUsername(String username)

	Long count()
}
