package tech.teslex.rbot.data.repository

import groovy.transform.CompileStatic
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import tech.teslex.rbot.data.model.User

@CompileStatic
@Repository
interface UserRepository extends JpaRepository<User, Long> {
	User findByUsername(String username)
}
