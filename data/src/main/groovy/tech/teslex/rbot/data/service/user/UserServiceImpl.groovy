package tech.teslex.rbot.data.service.user

import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import tech.teslex.rbot.data.model.User
import tech.teslex.rbot.data.repository.UserRepository

@CompileStatic
@Service
class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository

	@Override
	User save(User user) {
		user.updatedAt = new Date()
		return userRepository.save(user)
	}

	@Override
	void delete(Long id) {
		userRepository.deleteById(id)
	}

	@Override
	Boolean exists(Long id) {
		return userRepository.existsById(id)
	}

	@Override
	Optional<User> findById(Long id) {
		return userRepository.findById(id)
	}

	@Override
	Optional<User> findByUsername(String username) {
		return userRepository.findByUsername(username) as Optional<User>
	}

	@Override
	Long count() {
		return userRepository.count()
	}
}
