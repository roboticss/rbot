package tech.teslex.rbot.controller

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping

@Controller
class PanelController {

	@GetMapping('/panel/**')
	String home() {
		'panel'
	}
}
