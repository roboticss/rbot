package tech.teslex.rbot.controller

import groovy.json.JsonSlurper
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Async
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import tech.teslex.rbot.bot.RBot

@RestController
@CompileStatic
class BotController {

	@Autowired
	RBot bot

	@Autowired
	JsonSlurper jsonSlurper

	@PostMapping("/bot!secured")
	@Async("processExecutor")
	handle(@RequestBody String body) {
		try {
			bot.telegroo.solve(jsonSlurper.parse(body.bytes) as Map)
		} catch (e) {
			[error: e.message]
		}
	}
}
