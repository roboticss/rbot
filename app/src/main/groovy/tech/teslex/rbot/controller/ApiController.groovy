package tech.teslex.rbot.controller

import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import tech.teslex.rbot.bot.RBot

@CompileStatic
@RestController
@RequestMapping('/api')
class ApiController {

	@Autowired
	RBot bot

	@GetMapping('/commands')
	commands() {
		(bot.telegroo.handles.message as Map)
				.findAll { (it.key as String)[0] == '/' }
				.collect { it.key.toString() }
	}
}
