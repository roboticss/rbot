package tech.teslex.rbot

import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import tech.teslex.rbot.bot.RBot

@SpringBootApplication
@CompileStatic
class RBotApplication implements ApplicationRunner {

	@Autowired
	RBot bot

	static void main(String[] args) {
		SpringApplication.run RBotApplication, args
	}

	@Override
	void run(ApplicationArguments args) throws Exception {
		try {
			bot.start()
		} catch (e) {
			throw e
		}
	}
}
