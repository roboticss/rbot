window.csrfToken = function () {
	return document.head.querySelector('meta[name="csrf-token"]').content;
}

window.toParam = function (obj) {
	return Object.entries(obj).map(([key, val]) => `${key}=${encodeURIComponent(val)}`).join('&')
}

window.requestForm = function (url, method, params = {}) {
	params = Object.assign(params, {
		method: method
	})
	params.headers = Object.assign(params.headers ? params.headers : {}, {
		'Content-Type': 'application/x-www-form-urlencoded'
	})

	params.body = Object.assign(params.body ? params.body : {}, {
		'csrf-token': csrfToken()
	})

	params.body = toParam(params.body)

	return fetch(url, params)
}


window.requestJson = function (url, method, params = {}) {
	params = Object.assign(params, {
		method: method
	})
	params.headers = Object.assign(params.headers ? params.headers : {}, {
		'Content-Type': 'application/json'
	})

	params.body = Object.assign(params.body ? params.body : {}, {
		'csrf-token': csrfToken()
	})

	params.body = JSON.stringify(params.body)

	return fetch(url, params)
}

window.postForm = function (url, params = {}) {
	return window.requestForm(url, 'post', params)
}

window.putForm = function (url, params = {}) {
	return window.requestForm(url, 'put', params)
}

window.patchForm = function (url, params = {}) {
	return window.requestForm(url, 'patch', params)
}

window.deleteForm = function (url, params = {}) {
	return window.requestForm(url, 'delete', params)
}


window.postJson = function (url, params = {}) {
	return window.requestJson(url, 'post', params)
}

window.putJson = function (url, params = {}) {
	return window.requestJson(url, 'put', params)
}

window.patchJson = function (url, params = {}) {
	return window.requestJson(url, 'patch', params)
}

window.deleteJson = function (url, params = {}) {
	return window.requestJson(url, 'delete', params)
}


