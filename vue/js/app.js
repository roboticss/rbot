require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begins adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import router from './router'

import KeenUI from 'keen-ui'
import 'keen-ui/dist/keen-ui.css'
// import 'font-awesome/css/font-awesome.min.css'

Vue.use(KeenUI)

const admin = new Vue({
	el: '#app',
	router
});
